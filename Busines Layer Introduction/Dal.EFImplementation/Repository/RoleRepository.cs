﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dal.Interface.Entities;
using Dal.Interface.Interfaces;

namespace Dal.EFImplementation.Repository
{
    public class RoleRepository: IRoleRepository
    {
        public RoleRepository(EntitiesContext context)
        {
            _context = context;
        }

        private readonly EntitiesContext _context;

        public IEnumerable<RoleDto> GetAll()
        {
            return _context.Roles.ToList();
        }

        public RoleDto GetSingle(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RoleDto> Find(params Expression<Func<RoleDto, bool>>[] predicates)
        {
            throw new NotImplementedException();
        }

        public void Add(RoleDto entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(RoleDto entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(RoleDto entity)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
