﻿using System.Collections.Generic;

namespace Dal.Interface.Entities
{
    public class UserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public virtual List<RoleDto> Roles { get; set; }
    }
}
