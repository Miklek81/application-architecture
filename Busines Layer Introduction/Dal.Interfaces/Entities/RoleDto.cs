﻿using System.Collections.Generic;

namespace Dal.Interface.Entities
{
    public class RoleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual List<UserDto> Users { get; set; }
    }
}
