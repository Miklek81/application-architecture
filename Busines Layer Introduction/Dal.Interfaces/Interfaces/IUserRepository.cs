﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Dal.Interface.Entities;

namespace Dal.Interface.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<UserDto> GetAll();
        UserDto GetSingle(long id);
        IEnumerable<UserDto> Find(params Expression<Func<UserDto, bool>>[] predicates);

        void Add(UserDto entity);
        void Edit(UserDto entity);
        void Delete(UserDto entity);
        void Save();
    }
}
