﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Bll.Interface.Entities;

namespace Bll.Interface.DataServices
{
    public interface IUserService
    {
        void LockUser(long userId);
        void UnlockUser(long userId);
        void AddRoles(long userId, IEnumerable<string> roles);
        void RemoveRole(long userId, long roleId);

        IEnumerable<User> GetAll();
        User GetSingle(long id);
        IEnumerable<User> Find(params Expression<Func<User, bool>>[] predicates);

        void Add(User entity);
        void Edit(User entity);
        void Delete(User entity);
        void Save();
    }
}
