﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Bll.Interface.Entities;

namespace Bll.Interface.DataServices
{
    public interface IRoleService
    {
        IEnumerable<Role> GetAll();
        Role GetSingle(long id);
        IEnumerable<Role> Find(params Expression<Func<Role, bool>>[] predicates);

        void Add(Role entity);
        void Edit(Role entity);
        void Delete(Role entity);
        void Save();
    }
}
