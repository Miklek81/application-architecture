﻿using System.Collections.Generic;

namespace Bll.Interface.Entities
{
    public class User
    {
        public long Id { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public List<Role> Roles { get; set; }
    }
}
