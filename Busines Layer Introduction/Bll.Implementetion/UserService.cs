﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Bll.Implementetion.Mappers;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.Entities;
using Dal.Interface.Interfaces;

namespace Bll.Implementetion
{
    public class UserService : IUserService
    {
        public UserService(IUserRepository usersRepository, IUnitOfWork unitOfWork, IRoleRepository rolesRepository)
        {
            _usersRepository = usersRepository;
            UnitOfWork = unitOfWork;
            _rolesRepository = rolesRepository;
        }

        private readonly IUserRepository _usersRepository;
        private readonly IRoleRepository _rolesRepository;

        protected readonly IUnitOfWork UnitOfWork;
        protected readonly UserMapper Mapper = new UserMapper();


        public virtual void AddRoles(long userId, IEnumerable<string> roles)
        {
            UserDto user = _usersRepository.GetSingle(userId);
            if (user != null)
            {
                if (user.Roles == null)
                {
                    user.Roles = new List<RoleDto>();
                }
                foreach (var role in roles)
                {
                    RoleDto roleDto = _rolesRepository.GetAll().FirstOrDefault(r => r.Name == role);
                    if (roleDto == null)
                    {
                        roleDto = new RoleDto() { Name = role };
                        _rolesRepository.Add(roleDto);
                    }
                    if (user.Roles.All(r => r.Name != role))
                    {
                        user.Roles.Add(roleDto);
                    }
                }
            }
            UnitOfWork.Commit();
        }

        public virtual IEnumerable<User> GetAll()
        {
            IEnumerable<User> users = _usersRepository.GetAll().Select(Mapper.GetBllEntity);
            return users;
        }

        public virtual void LockUser(long userId)
        {
            throw new NotImplementedException();
        }

        public virtual void UnlockUser(long userId)
        {
            throw new NotImplementedException();
        }

        

        public virtual void RemoveRole(long userId, long roleId)
        {
            throw new NotImplementedException();
        }

        

        public virtual User GetSingle(long id)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<User> Find(params Expression<Func<User, bool>>[] predicates)
        {
            throw new NotImplementedException();
        }

        public virtual void Add(User entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Edit(User entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Save()
        {
            throw new NotImplementedException();
        }
    }
}
