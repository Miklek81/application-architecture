﻿using System.Collections.Generic;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.Interfaces;

namespace Bll.Implementetion.SingleOperationPerUnitOfWorkServices
{
    public class SingleOperationPerUnitOfWorkUserService: UserService
    {
        public SingleOperationPerUnitOfWorkUserService(IUserRepository usersRepository, IUnitOfWork unitOfWork, IRoleRepository rolesRepository) : base(usersRepository, unitOfWork, rolesRepository)
        {
        }

        public override void AddRoles(long userId, IEnumerable<string> roles)
        {
            base.AddRoles(userId, roles);
            UnitOfWork.Dispose();
        }

        public override IEnumerable<User> GetAll()
        {
            var result = base.GetAll();
            UnitOfWork.Dispose();
            return result;
        }
    }
}
