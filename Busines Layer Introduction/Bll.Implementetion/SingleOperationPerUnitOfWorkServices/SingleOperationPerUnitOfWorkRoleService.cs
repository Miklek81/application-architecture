﻿using System.Collections.Generic;
using Dal.Interface;
using Dal.Interface.Interfaces;

namespace Bll.Implementetion.SingleOperationPerUnitOfWorkServices
{
    public class SingleOperationPerUnitOfWorkRoleService: RoleService
    {
        public SingleOperationPerUnitOfWorkRoleService(IRoleRepository repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }

        public override IEnumerable<Interface.Entities.Role> GetAll()
        {
            var result = base.GetAll();
            UnitOfWork.Dispose();
            return result;
        }

    }
}
