﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.Interfaces;

namespace Bll.Implementetion
{
    public class RoleService: IRoleService
    {
        public RoleService(IRoleRepository repository, IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            RoleRepository = repository;
        }

        protected readonly IUnitOfWork UnitOfWork;

        protected readonly IRoleRepository RoleRepository;

        public virtual IEnumerable<Role> GetAll()
        {
            throw new NotImplementedException();
        }

        public virtual Role GetSingle(long id)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<Role> Find(params Expression<Func<Role, bool>>[] predicates)
        {
            throw new NotImplementedException();
        }

        public virtual void Add(Role entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Edit(Role entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(Role entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Save()
        {
            throw new NotImplementedException();
        }
    }
}
