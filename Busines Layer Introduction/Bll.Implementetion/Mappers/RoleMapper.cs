﻿using Bll.Interface.Entities;
using Dal.Interface.Entities;

namespace Bll.Implementetion.Mappers
{
    public class RoleMapper
    {
        public Role GetBllEntity(RoleDto dalEntity)
        {
            return new Role()
            {
                Id = dalEntity.Id,
                Name = dalEntity.Name
            };
        }

        public RoleDto GetDalEntity(Role bllEntity)
        {
            return new RoleDto()
            {
                Id = bllEntity.Id,
                Name = bllEntity.Name
            };
        }
    }
}
