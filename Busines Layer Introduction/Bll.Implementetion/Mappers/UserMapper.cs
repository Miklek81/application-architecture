﻿using System.Linq;
using Bll.Interface.Entities;
using Dal.Interface.Entities;

namespace Bll.Implementetion.Mappers
{
    public class UserMapper
    {
        public User GetBllEntity(UserDto dalEntity)
        {
            return new User()
            {
                Id = dalEntity.Id,
                Name = dalEntity.Name,
                Password = dalEntity.Password,
                Roles = dalEntity.Roles !=null ? dalEntity.Roles.Select( r => new Role() { Id = r.Id, Name = r.Name } ).ToList() : null
            };
        }

        public UserDto GetDalEntity(User bllEntity)
        {
            return new UserDto()
            {
                Id = bllEntity.Id,
                Name = bllEntity.Name,
                Password = bllEntity.Password,
                Roles = bllEntity.Roles.Select(r => new RoleDto() {Id = r.Id, Name = r.Name}).ToList()
            };
        }
    }
}
