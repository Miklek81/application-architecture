﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            ConfigreAppDataDirectory();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        private static void ConfigreAppDataDirectory()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            int index = baseDir.IndexOf("ApplicationArchitecture");
            string dataDir = baseDir.Substring(0, index) + @"ApplicationArchitecture\Data Directory";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
        }
    }
}
