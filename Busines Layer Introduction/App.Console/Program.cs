﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;
using Ninject;

namespace App.Console
{
    class Program
    {
        static Program()
        {
            _resolver = new StandardKernel();
            _resolver.Configure();
        }

        private readonly static IKernel _resolver;

        static void Main(string[] args)
        {
            ConfigreAppDataDirectory();
            
            DisplayUserList();
            System.Console.ReadKey();
        }

        static void DisplayUserList()
        {
            IUserService userService = _resolver.Get<IUserService>();

            IEnumerable<User> users = userService.GetAll();
            foreach (var user in users)
            {
                System.Console.Write(user.Name);
                if (user.Roles != null)
                {
                    if (user.Roles.Any())
                    {
                        System.Console.Write(" has roles:");
                    }
                    foreach (var role in user.Roles)
                    {
                        System.Console.Write(" {0}", role.Name);
                    }
                }
                System.Console.WriteLine();
            }
        }

        static void AddRoles(long userId, IEnumerable<string> roles)
        {
            IUserService userService = _resolver.Get<IUserService>();
            userService.AddRoles(userId, roles);
        }

        private static void ConfigreAppDataDirectory()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            int index = baseDir.IndexOf("ApplicationArchitecture");
            string dataDir = baseDir.Substring(0, index) + @"ApplicationArchitecture\Data Directory";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
        }
    }
}
