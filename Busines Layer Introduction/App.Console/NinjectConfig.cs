﻿using System.Data.Entity;
using Bll.Implementetion.SingleOperationPerUnitOfWorkServices;
using Bll.Interface.DataServices;
using Dal.EFImplementation;
using Dal.EFImplementation.Repository;
using Dal.Interface;
using Dal.Interface.Interfaces;
using Ninject;

namespace App.Console
{
    static class NinjectConfig
    {
        public static void Configure(this IKernel kernel)
        {
            kernel.Bind<DbContext>().To<EntitiesContext>();

            kernel.Bind<IRoleRepository>().To<RoleRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();

            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            kernel.Bind<IRoleService>().To<SingleOperationPerUnitOfWorkRoleService>();
            kernel.Bind<IUserService>().To<SingleOperationPerUnitOfWorkUserService>();
        }

    }
}
