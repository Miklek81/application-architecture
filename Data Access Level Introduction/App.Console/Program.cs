﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dal.Interface;
using Dal.Interface.Entities;
using Dal.Interface.Interfaces;
using Ninject;

namespace App.Console
{
    class Program
    {
        static Program()
        {
            _resolver = new StandardKernel();
            _resolver.Configure();
        }

        private readonly static IKernel _resolver;

        static void Main(string[] args)
        {
            ConfigreAppDataDirectory();

            DisplayUserList();
            System.Console.ReadKey();
        }

        static void DisplayUserList()
        {
            IUserRepository userRepository = _resolver.Get<IUserRepository>();

            IEnumerable<UserDto> users = userRepository.GetAll();
            foreach (var user in users)
            {
                System.Console.Write(user.Name);
                if (user.Roles != null)
                {
                    if (user.Roles.Any())
                    {
                        System.Console.Write(" has roles:");
                    }
                    foreach (var role in user.Roles)
                    {
                        System.Console.Write(" {0}", role.Name);
                    }
                }
                System.Console.WriteLine();
            }
        }

        static void AddRoles(long userId, IEnumerable<string> roles)
        {
            IRoleRepository rolesRepository = _resolver.Get<IRoleRepository>();
            IUserRepository userRepository = _resolver.Get<IUserRepository>();

            IUnitOfWork unitOfWork = _resolver.Get<IUnitOfWork>();

            UserDto user = userRepository.GetSingle(userId);
            if (user != null)
            {
                if (user.Roles == null)
                {
                    user.Roles = new List<RoleDto>();
                }
                foreach (var role in roles)
                {
                    RoleDto roleDto = rolesRepository.GetAll().FirstOrDefault(r => r.Name == role);
                    if (roleDto == null)
                    {
                        roleDto = new RoleDto() {Name = role};
                        rolesRepository.Add(roleDto);
                    }
                    if (user.Roles.All(r => r.Name != role))
                    {
                        user.Roles.Add(roleDto);
                    }
                }
            }
            unitOfWork.Commit();
        }

        

        private static void ConfigreAppDataDirectory()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            int index = baseDir.IndexOf("ApplicationArchitecture");
            string dataDir = baseDir.Substring(0, index) + @"ApplicationArchitecture\Data Directory";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
        }
    }
}
