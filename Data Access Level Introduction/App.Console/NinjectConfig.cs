﻿using Dal.EFImplementation;
using Dal.EFImplementation.Repository;
using Dal.Interface;
using Dal.Interface.Interfaces;
using Ninject;

namespace App.Console
{
    static class NinjectConfig
    {
        public static void Configure(this IKernel kernel)
        {
            kernel.Bind<EntitiesContext>().ToSelf().InSingletonScope();

            kernel.Bind<IRoleRepository>().To<RoleRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();

            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
        }

    }
}
