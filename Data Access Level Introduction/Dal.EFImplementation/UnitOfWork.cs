﻿using System.Data.Entity;
using Dal.Interface;

namespace Dal.EFImplementation
{
    public class UnitOfWork: IUnitOfWork
    {
        public UnitOfWork(EntitiesContext context)
        {
            _context = context;
        }

        private readonly DbContext _context;

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
