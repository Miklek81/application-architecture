﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dal.Interface.Entities;
using Dal.Interface.Interfaces;

namespace Dal.EFImplementation.Repository
{
    public class UserRepository: IUserRepository
    {
        public UserRepository(EntitiesContext context)
        {
            _context = context;
        }

        private readonly EntitiesContext _context;

        public IEnumerable<UserDto> GetAll()
        {
            return _context.Users.ToList();
        }

        public UserDto GetSingle(long id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<UserDto> Find(params Expression<Func<UserDto, bool>>[] predicates)
        {
            throw new NotImplementedException();
        }

        public void Add(UserDto entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(UserDto entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(UserDto entity)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
