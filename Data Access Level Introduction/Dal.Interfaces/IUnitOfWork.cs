﻿namespace Dal.Interface
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
