﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Dal.Interface.Entities;

namespace Dal.Interface.Interfaces
{
    public interface IRoleRepository
    {
        IEnumerable<RoleDto> GetAll();
        RoleDto GetSingle(long id);
        IEnumerable<RoleDto> Find(params Expression<Func<RoleDto, bool>>[] predicates);

        void Add(RoleDto entity);
        void Edit(RoleDto entity);
        void Delete(RoleDto entity);
        void Save();
    }
}
