﻿using System.Data.Entity;

namespace App.Console
{
    public class EntitiesContext: DbContext
    {
        public EntitiesContext(): base("name=Demo") { }
        public DbSet<UserDto> Users { get; set; }
        public DbSet<RoleDto> Roles { get; set; }
    }
}
