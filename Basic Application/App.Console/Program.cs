﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Console
{
    class Program
    {

        static void Main(string[] args)
        {
            ConfigreAppDataDirectory();
            //AddRoles(1, new[] { "Admin", "Root" });
            DisplayUserList();
            System.Console.ReadKey();
        }

        static void DisplayUserList()
        {
            using (var context = new EntitiesContext())
            {
                IEnumerable<UserDto> users = context.Users.ToList();
                foreach (var user in users)
                {
                    System.Console.Write(user.Name);
                    if (user.Roles != null)
                    {
                        if (user.Roles.Any())
                        {
                            System.Console.Write(" has roles:");
                        }
                        foreach (var role in user.Roles)
                        {
                            System.Console.Write(" {0}", role.Name);
                        }
                    }
                    System.Console.WriteLine();
                }
            }
        }

        static void AddRoles(long userId, IEnumerable<string> roles)
        {
            using (var context = new EntitiesContext())
            {
                UserDto user = context.Users.FirstOrDefault(u => u.Id == userId);
                if (user != null)
                {
                    if (user.Roles == null)
                    {
                        user.Roles = new List<RoleDto>();
                    }
                    foreach (var role in roles)
                    {
                        RoleDto roleDto = context.Roles.FirstOrDefault(r => r.Name == "role");
                        if (roleDto == null)
                        {
                            roleDto = new RoleDto() {Name = role};
                            context.Roles.Add(roleDto);
                        }
                        if (user.Roles.All(r => r.Name != role))
                        {
                            user.Roles.Add(roleDto);
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        private static void ConfigreAppDataDirectory()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            int index = baseDir.IndexOf("ApplicationArchitecture");
            string dataDir = baseDir.Substring(0, index) + @"ApplicationArchitecture\Data Directory";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
        }
    }
}
