﻿using System.Collections.Generic;

namespace App.Console
{
    public class UserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public virtual List<RoleDto> Roles { get; set; }
    }
}
