﻿using System.Collections.Generic;

namespace App.Console
{
    public class RoleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual List<UserDto> Users { get; set; }
    }
}
