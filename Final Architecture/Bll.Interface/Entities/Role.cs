﻿using System.Collections.Generic;
using Core;

namespace Bll.Interface.Entities
{
    public class Role: IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
