﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Core;

namespace Bll.Interface
{
    public interface IService<TEntity> where TEntity: class, IEntity
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetSingle(long id);
        IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] predicates);

        void Add(TEntity entity);
        void Edit(TEntity entity);
        void Delete(TEntity entity);
        void Save();
    }
}
