﻿using System.Collections.Generic;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.DataAccess;

namespace Bll.Implementation.SngleOperationPerUnitfWorkServces
{
    public class SingleOperatonPerUnitOfWorkUserService: UserService
    {
        public SingleOperatonPerUnitOfWorkUserService(IUserRepository repository, IRoleRepository roleRepository, IUnitOfWork unitOfWork) : base(repository, roleRepository, unitOfWork)
        {
        }

        public override IEnumerable<User> GetAll()
        {
            var result = base.GetAll();
            UnitOfWork.Dispose();
            return result;
        }

        public override void AddRoles(long userId, IEnumerable<string> roles)
        {
            base.AddRoles(userId, roles);
            UnitOfWork.Dispose();
        }
    }
}
