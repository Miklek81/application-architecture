﻿using System.Collections.Generic;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.DataAccess;

namespace Bll.Implementation.SngleOperationPerUnitfWorkServces
{
    public class SingeOperationPerUnitOfWorkRoleService: RoleService
    {
        public SingeOperationPerUnitOfWorkRoleService(IRoleRepository repository, IUnitOfWork unitOfWork) 
            :base(repository, unitOfWork)
        {
        }

        public override IEnumerable<Role> GetAll()
        {
            var result = base.GetAll();
            UnitOfWork.Dispose();
            return result;
        }
    }
}
