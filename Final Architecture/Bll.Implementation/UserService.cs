﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Implementation.Infrastructure;
using Bll.Implementation.Infrastructure.Mappers;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.DataAccess;
using Dal.Interface.Entities;

namespace Bll.Implementation
{
    public class UserService : Service<User, UserDto, IUserRepository, UserMapper>, IUserService
    {
        public UserService(IUserRepository repository, IRoleRepository roleRepository, IUnitOfWork unitOfWork) :base(repository, unitOfWork)
        {
            _roleRepository = roleRepository;
        }

        private readonly IRoleRepository _roleRepository;

        public virtual void AddRoles(long userId, IEnumerable<string> roles)
        {
            UserDto user = Repository.GetSingle(userId);
            if (user != null)
            {
                if (user.Roles == null)
                {
                    user.Roles = new List<RoleDto>();
                }
                foreach (var role in roles)
                {
                    RoleDto roleDto = _roleRepository.GetAll().FirstOrDefault(r => r.Name == role);
                    if (roleDto == null)
                    {
                        roleDto = new RoleDto() { Name = role };
                        _roleRepository.Add(roleDto);
                    }
                    if (user.Roles.All(r => r.Name != role))
                    {
                        user.Roles.Add(roleDto);
                    }
                }
            }
            UnitOfWork.Commit();
        }

        public virtual void LockUser(long userId)
        {
            throw new NotImplementedException();
        }

        public virtual void UnlockUser(long userId)
        {
            throw new NotImplementedException();
        }

        public virtual void RemoveRole(long userId, long roleId)
        {
            throw new NotImplementedException();
        }
    }
}
