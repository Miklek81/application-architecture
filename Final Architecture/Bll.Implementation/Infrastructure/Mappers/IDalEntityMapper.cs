﻿using Core;

namespace Bll.Implementation.Infrastructure.Mappers
{
    public interface IDalEntityMapper<TBllEntity, TDalEntity>
        where TBllEntity : IEntity
        where TDalEntity : IEntity
    {
        TBllEntity GetBllEntity(TDalEntity dalEntity);
        TDalEntity GetDalEntity(TBllEntity bllEntity);
    }
}
