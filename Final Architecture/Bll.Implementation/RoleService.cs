﻿using Bll.Implementation.Infrastructure;
using Bll.Implementation.Infrastructure.Mappers;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;
using Dal.Interface;
using Dal.Interface.DataAccess;
using Dal.Interface.Entities;

namespace Bll.Implementation
{
    public class RoleService : Service<Role, RoleDto, IRoleRepository, RoleMapper>, IRoleService
    {
        public RoleService(IRoleRepository repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}
