﻿using System;

namespace Dal.Interface
{
    public interface IUnitOfWork: IDisposable
    {
        void Commit();
    }
}
