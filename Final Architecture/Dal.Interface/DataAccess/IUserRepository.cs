﻿using Dal.Interface.Entities;

namespace Dal.Interface.DataAccess
{
    public interface IUserRepository: IEntityRepository<UserDto>
    {
    }
}
