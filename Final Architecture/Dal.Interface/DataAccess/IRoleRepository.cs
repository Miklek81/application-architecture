﻿using Dal.Interface.Entities;

namespace Dal.Interface.DataAccess
{
    public interface IRoleRepository: IEntityRepository<RoleDto>
    {
    }
}
