﻿using System.Collections.Generic;
using Core;

namespace Dal.Interface.Entities
{
    public class RoleDto: IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual List<UserDto> Users { get; set; }
    }
}
