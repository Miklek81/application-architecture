﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Core;

namespace Dal.Interface.DataAccess
{
    public interface IEntityRepository<TDto> where TDto : class, IEntity
    {
        IEnumerable<TDto> GetAll();
        TDto GetSingle(long id);
        IEnumerable<TDto> Find(params Expression<Func<TDto, bool>>[] predicates);

        void Add(TDto entity);
        void Edit(TDto entity);
        void Delete(TDto entity);
        void Save();
    }
}
