﻿using System.Data.Entity;
using Dal.Interface.DataAccess;
using Dal.Interface.Entities;

namespace Dal.EFImplementation.Concrete
{
    public class RoleRepository: EntityRepository<RoleDto>, IRoleRepository
    {
        public RoleRepository(DbContext entitiesContext) : base(entitiesContext) { }
    }
}
