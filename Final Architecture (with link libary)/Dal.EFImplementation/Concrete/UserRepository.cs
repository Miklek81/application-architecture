﻿using System.Data.Entity;
using Dal.Interface.DataAccess;
using Dal.Interface.Entities;

namespace Dal.EFImplementation.Concrete
{
    public class UserRepository: EntityRepository<UserDto>, IUserRepository
    {
        public UserRepository(DbContext entitiesContext) : base(entitiesContext) { }
    }
}
