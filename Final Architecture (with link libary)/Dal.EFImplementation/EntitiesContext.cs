﻿using System.Data.Entity;
using Dal.Interface.Entities;

namespace Dal.EFImplementation
{
    public class EntitiesContext : DbContext
    {
        public EntitiesContext(): base("name=Demo") { }

        public DbSet<UserDto> TaskLists { get; set; }
        public DbSet<RoleDto> Comments { get; set; }
    }
}
