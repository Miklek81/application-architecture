﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Core;
using Dal.Interface.DataAccess;

namespace Dal.EFImplementation
{
    public class EntityRepository<T> : IEntityRepository<T>
        where T : class, IEntity, new()
    {

        readonly DbContext _entitiesContext;

        public EntityRepository(DbContext entitiesContext)
        {
            if (entitiesContext == null)
            {
                throw new ArgumentNullException("entitiesContext");
            }
            _entitiesContext = entitiesContext;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _entitiesContext.Set<T>().AsEnumerable();
        }

        public T GetSingle(long id)
        {
            return _entitiesContext.Set<T>().FirstOrDefault(x => x.Id == id);
        }

        public virtual IEnumerable<T> Find(params Expression<Func<T, bool>>[] predicates)
        {
            IQueryable<T> tmp = _entitiesContext.Set<T>().AsQueryable();
            tmp = predicates.Aggregate(tmp, (current, predicate) => current.Where(predicate));
            return tmp.AsEnumerable();
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry<T> dbEntityEntry = _entitiesContext.Entry<T>(entity);
            _entitiesContext.Set<T>().Add(dbEntityEntry.Entity);
        }

        public virtual void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = _entitiesContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = _entitiesContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public virtual void Save()
        {
            _entitiesContext.SaveChanges();
        }

    }
}
