﻿using System.Collections.Generic;
using System.Web.Mvc;
using Bll.Interface.DataServices;
using Bll.Interface.Entities;

namespace App.Web.Controllers
{
    public class UserController : Controller
    {
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        private readonly IUserService _userService;
        //
        // GET: /User/
        public ActionResult Index()
        {
            IEnumerable<User> users = _userService.GetAll();
            return View(users);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddRoles(long userId, IEnumerable<string> roles)
        {
            _userService.AddRoles(userId, roles);
            return RedirectToAction("Index");
        }
	}
}