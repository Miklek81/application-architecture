﻿using System.Data.Entity;
using Bll.Implementation;
using Bll.Implementation.SngleOperationPerUnitfWorkServces;
using Bll.Interface.DataServices;
using Dal.Interface;
using Dal.Interface.DataAccess;
using Ninject;
using Ninject.Web.Common;
using Dal.EFImplementation;
using Dal.EFImplementation.Concrete;

namespace DependencyResolver
{
    public static class ResolverConfiguration
    {
        public static void ConfigureWeb(this IKernel kernel)
        {
            Configure(kernel, true);
        }

        public static void ConfigureConsole(this IKernel kernel)
        {
            Configure(kernel, false);
        }

        private static void Configure(IKernel kernel, bool isWeb)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            if (isWeb)
            {
                kernel.Bind<DbContext>().To<EntitiesContext>().InRequestScope();
                kernel.Bind<IRoleService>().To<RoleService>();
                kernel.Bind<IUserService>().To<UserService>();
            }
            else
            {
                kernel.Bind<DbContext>().To<EntitiesContext>();
                kernel.Bind<IRoleService>().To<SingeOperationPerUnitOfWorkRoleService>();
                kernel.Bind<IUserService>().To<SingleOperatonPerUnitOfWorkUserService>();
            }

            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IRoleRepository>().To<RoleRepository>();
            
        }
    }
}
