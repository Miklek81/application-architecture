﻿using System.Collections.Generic;
using Core;

namespace Bll.Interface.Entities
{
    public class User: IEntity
    {
        public long Id { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public List<Role> Roles { get; set; }
    }
}
