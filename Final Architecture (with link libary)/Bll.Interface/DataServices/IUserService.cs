﻿using System.Collections;
using System.Collections.Generic;
using Bll.Interface.Entities;

namespace Bll.Interface.DataServices
{
    public interface IUserService : IService<User>
    {
        void AddRoles(long userId, IEnumerable<string> roles);
        void LockUser(long userId);
        void UnlockUser(long userId);
        void RemoveRole(long userId, long roleId);
    }
}
