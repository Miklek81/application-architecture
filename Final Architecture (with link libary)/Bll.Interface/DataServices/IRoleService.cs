﻿using Bll.Interface.Entities;

namespace Bll.Interface.DataServices
{
    public interface IRoleService: IService<Role> { }
}
