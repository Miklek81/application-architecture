﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Bll.Implementation.Infrastructure.Mappers;
using Bll.Interface;
using Core;
using Dal.Interface;
using Dal.Interface.DataAccess;

namespace Bll.Implementation.Infrastructure
{
    public abstract class BaseBaseService<TEntity, TDto, TRepository, TEntityMapper>: IService<TEntity> 
        where TEntity: class, IEntity
        where TDto: class, IEntity
        where TRepository: IEntityRepository<TDto>
        where TEntityMapper: IDalEntityMapper<TEntity, TDto>, new()
    {
        public BaseBaseService(TRepository repository, IUnitOfWork unitOfWork)
        {
            Repository = repository;
            UnitOfWork = unitOfWork;
        }

        public readonly TRepository Repository;
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly IDalEntityMapper<TEntity, TDto> EntityMapper = new TEntityMapper();
        

        protected TEntity GetEntity(TDto dto)
        {
            var entity = EntityMapper.GetBllEntity(dto);
            return entity;
        }
        protected TDto GetDto(TEntity entity)
        {
            TDto dto = EntityMapper.GetDalEntity(entity);
            return dto;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll().Select(GetEntity);
        }
        public virtual TEntity GetSingle(long id)
        {
            TDto dto = Repository.GetSingle(id);
            return GetEntity(dto);
        }
        public virtual IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] predicates)
        {
            throw new NotImplementedException();
            //return Repository.Find(predicates);
        }

        public virtual void Add(TEntity entity)
        {
            TDto dto = GetDto(entity);
            Repository.Add(dto);
            UnitOfWork.Commit();
        }
        public virtual void Edit(TEntity entity)
        {
            TDto dto = GetDto(entity);
            Repository.Edit(dto);
            UnitOfWork.Commit();
        }
        public virtual void Delete(TEntity entity)
        {
            TDto dto = GetDto(entity);
            Repository.Delete(dto);
            UnitOfWork.Commit();
        }
        public void Save()
        {
            Repository.Save();
        }
    }
}
