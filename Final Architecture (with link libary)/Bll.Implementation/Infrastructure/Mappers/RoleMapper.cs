﻿using Bll.Interface.Entities;
using Dal.Interface.Entities;

namespace Bll.Implementation.Infrastructure.Mappers
{
    public class RoleMapper: IDalEntityMapper<Role, RoleDto>
    {
        public Role GetBllEntity(RoleDto dalEntity)
        {
            return new Role()
            {
                Id = dalEntity.Id,
                Name = dalEntity.Name
            };
        }

        public RoleDto GetDalEntity(Role bllEntity)
        {
            return new RoleDto()
            {
                Id = bllEntity.Id,
                Name = bllEntity.Name
            };
        }
    }
}
